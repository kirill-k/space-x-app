/* eslint-disable */
import React from 'react';
import Main from './components/main'
import Capsule from './components/capsule'
import Dragons from './components/dragon'
import Dragon from './components/dragon/dragon'
import Favorite from './components/dragon/favorite'
import NotFound from './components/notFound'


export const Routes = [
    {
        path: '/',
        exact: true,
        component: Main,
    },
    {
        path: '/capsules',
        exact: true,
        component: Capsule,
    },
    {
        path: '/dragons',
        exact: true,
        component: Dragons,
    },
    {
        path: '/dragons/:id',
        exact: true,
        component: Dragon,
    },
    {
        path: '/favorites',
        exact: true,
        component: Favorite,
    },
    {
        exact: true,
        component: NotFound
    }
];