/* eslint-disable */
import React from 'react'
import * as actionTypes from '../constants/capsule';
import axios from 'axios';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const getDataDone = data => {
    return {
        type: actionTypes.CAPSULE_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.CAPSULE_ERROR
    };
};

export const getCapsuleData = () => dispatch => {
    const url = 'https://api.spacexdata.com/v3/capsules';
    dispatch(getDataRequested());

    axios.get(url)
        .then(res => {
            const data = res.data;
            dispatch(getDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
}