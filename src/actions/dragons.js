/* eslint-disable */
import React from 'react'
import * as actionTypes from '../constants/dragon';
import axios from 'axios';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DRAGONS_REQUESTED
    };
};

const getDataDone = data => {
    return {
        type: actionTypes.DRAGONS_SUCCESS,
        data: data
    };
};

const getDataDragonDone = data => {
    return {
        type: actionTypes.DRAGON_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.DRAGONS_ERROR
    };
};

const setDataToLocalStorage = data => {
    return {
        type: actionTypes.DRAGON_TO_LOCAL_STORAGE,
        data: data
    };
};

const setDataFromLocalStorage = data => {
    return {
        type: actionTypes.DRAGON_FROM_LOCAL_STORAGE,
        data: data
    };
};

export const getDragonsData = () => dispatch => {
    const url = 'https://api.spacexdata.com/v3/dragons';

    dispatch(getDataRequested());

    axios.get(url)
        .then(res => {
            const data = res.data;
            dispatch(getDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
};

export const getDragonItemData = (id) => dispatch => {
    const url = `https://api.spacexdata.com/v3/dragons/${id}`;
    dispatch(getDataRequested());

    axios.get(url)
        .then(res => {
            const data = res.data;
            dispatch(getDataDragonDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
};

export const setDragonToLocalStorage = (data) => dispatch => {
    dispatch(setDataToLocalStorage(data));
};

export const removeDragonFromLocalStorage = (data) => dispatch => {
    dispatch(setDataFromLocalStorage(data));
};
