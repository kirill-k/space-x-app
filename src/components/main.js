/* eslint-disable */
import React, { Component } from 'react';
import mainBanner from '../assets/images/the-spacex-falcon-9.jpg'
import '../index.css';


class Main extends Component {

    state = {
        showString: true
    };

    render() {
        const style = {
            title: {
                height: '100%',
                width: '100vw',
                background: `url(${mainBanner}) no-repeat center center fixed`,
                backgroundSize: 'cover',
                position: 'absolute',
                top: 0,
                left: 0
            },
            styledTitle: {
                color: '#3e80b4'
            }
        };

        return (
            <div style={{...style.title}} className="main_text_block">
                <h2>
                    SpaceX designs, manufactures and launches advanced <br/>
                    <strong style={{...style.styledTitle}}>rockets and spacecraft</strong>
                </h2>
            </div>
        )
    }

}

export default Main;
