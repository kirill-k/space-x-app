/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Loading from '../common/loader';
import { connect } from 'react-redux';
import * as dragonsActions from '../../actions/dragons';
import 'react-table/react-table.css'

const styles = {
    wrapper: {
        padding: 20,
        textAlign: 'center',
        width: '100%',
        margin: 'auto',
        marginTop: 60
    },
    container: {
        display:'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    block: {
        width: '50%'
    },
    image: {
        width: 600,
        margin: '0 0 20px'
    },
    title: {
        color: '#000',
        textAlign: 'center',
        paddingBottom: 25,
        fontSize: 22,
        fontWeight: 'bold'
    },
    favorite: {
        display:'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer',
        margin: '0 0 30px'
    },
    favoriteImage: {
        width: 20,
        paddingRight: 5
    }
};

class Dragons extends Component {

    state = {
        dragonInStorage: []
    };

    componentDidMount = () => {
        this.props.getDragonsData()
    };

    renderDragonCollection = () => {
        const { data, match } = this.props;

        return data.dragons.map((dragon, index) => {
            return (
                <div
                    key={index + dragon.dry_mass_kg}
                    style={styles.block}
                >
                    <NavLink to={`${match.url}/${dragon.id}`}>
                        <h3 style={styles.title}>{dragon.name}</h3>
                        <img style={styles.image} src={dragon.flickr_images[0]}/>
                    </NavLink>
                    <div
                        style={styles.favorite}
                        onClick={() => this.addToLocalStorage(dragon)}
                    >
                        <img
                            style={styles.favoriteImage}
                            src="https://icons-for-free.com/free-icons/png/512/1976054.png"
                            alt="favorite"
                        />
                        <span>Add to favorites</span>
                    </div>
                </div>
            )
        })
    };

    addToLocalStorage = (dragon) => {
        let toStorage = this.state.dragonInStorage;

        toStorage.indexOf(dragon) === -1 ?
            toStorage.push(dragon) :
            console.log("This item already exists");
        this.props.setDragonToLocalStorage(toStorage)

    };

    render() {
        const { data } = this.props;

        if (data.loading === true) {
            return <Loading />
        } else {
            return (

                <div style={styles.wrapper}>
                    <div style={styles.container}>
                        {this.renderDragonCollection()}
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.dragons
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDragonsData: data => dispatch(dragonsActions.getDragonsData(data)),
        setDragonToLocalStorage: data => dispatch(dragonsActions.setDragonToLocalStorage(data))
    };
};

Dragons.propTypes = {
    data: PropTypes.object,
    getDragonsData: PropTypes.func,
    setDragonToLocalStorage: PropTypes.func
};


export default connect(mapStateToProps, mapDispatchToProps)(Dragons);