/* eslint-disable */
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Loading from '../common/loader';
import { connect } from 'react-redux';
import * as dragonsActions from '../../actions/dragons';
import 'react-table/react-table.css'

const styles = {
    wrapper: {
        padding: 20,
        textAlign: 'center',
        width: '80%',
        margin: 'auto',
        marginTop: 60
    },
    container: {
        display:'flex',
        flexDirection: 'column',
        alignItems: 'center',
        position: 'relative',
        left: '20px'
    },
    block: {
        width: '40%'
    },
    image: {
        width: 600,
        margin: '0 0 20px'
    },
    title: {
        color: '#000',
        textAlign: 'center',
        paddingBottom: 25,
        fontSize: 22,
        fontWeight: 'bold'
    },
    favorite: {
        display:'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer',
        margin: '0 0 30px'
    },
    favoriteImage: {
        width: 20,
        paddingRight: 5
    }
};

class Favorite extends Component {

    state = {
        dragonInStorage: []
    };

    componentDidMount = () => {
        this.props.getDragonsData()
    };


    renderDragonCollection = () => {
        const { data } = this.props;

        if (data.localStorage && data.localStorage.length>0){
            return data.localStorage.map((dragon, index) => {
                return (
                    <div key={index + dragon.dry_mass_kg}>
                        <NavLink to={`dragons/${dragon.id}`}>
                            <h3 style={styles.title}>{dragon.name}</h3>
                            <img style={styles.image} src={dragon.flickr_images[0]}/>
                        </NavLink>
                        <div
                            style={styles.favorite}
                            onClick={() => this.removeFromLocalStorage(dragon)}
                        >
                            <img
                                style={styles.favoriteImage}
                                src="https://www.fincastleherald.com/wp-content/uploads/2017/11/trash_icon_1511960020.png"
                                alt="favorite"
                            />
                            <span>Remove from favorites</span>
                        </div>
                    </div>
                )
            })
        } else {
            return <h2>The favorite list is empty</h2>
        }
    };

    removeFromLocalStorage = (dragon) => {

        this.props.removeDragonFromLocalStorage(dragon)
    };

    render() {
        const { data } = this.props;

        if (data.loading === true) {
            return <Loading />
        } else {
            return (

                <div style={styles.wrapper}>
                    <div style={styles.container}>
                        {this.renderDragonCollection()}
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.dragons
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDragonsData: data => dispatch(dragonsActions.getDragonsData(data)),
        setDragonToLocalStorage: data => dispatch(dragonsActions.setDragonToLocalStorage(data)),
        removeDragonFromLocalStorage: data => dispatch(dragonsActions.removeDragonFromLocalStorage(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorite);