/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../common/loader';
import * as dragonsActions from '../../actions/dragons';
import { NavLink } from 'react-router-dom';
import '../../index.css'

const styles = {
    wrapper: {
        padding: 20,
        width: '80%',
        margin: 'auto',
        marginTop: 60
    },
    container: {
        display:'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
        left: '20px'
    },
    imageContainer: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    },
    image: {
        width: '75%',
        padding: '25px 0 25px'
    },
    title: {
        color: '#000',
        textAlign: 'center',
        paddingBottom: 25,
        fontSize: 25,
        fontWeight: 'bold'
    },
    link: {
        color: '#1a6494',
        fontWeight: 'bold',
        cursor: 'pointer'
    },
    detailsBlock: {
        paddingTop: 30
    }
};

class Dragon extends Component {

    state = {
        showDetails: false
    };

    componentDidMount = () => {
        const { match } = this.props;
        const id = match.params.id;
        this.props.getDragonItemData(id)
    };


    renderDragonImages = () => {
        const { data } = this.props;
        const images = data.dragon.flickr_images;

        if (images && images.length > 0) {
            return images.map((image, index) => {
                return (
                        <img
                            key={index}
                            style={styles.image}
                            src={image}
                            alt="dragon"
                        />
                )
            })
        }
    };

    renderDetails = () => {
        const { data } = this.props;


        if (this.state.showDetails){
            return(
                <div style={styles.detailsBlock}>
                    <p>{`Status: ${data.dragon.active === false ?
                        'Not Active' : (data.dragon.active === true ?
                        'Active' : data.dragon.active)}`}
                    </p>
                    <p>{`First flight: ${data.dragon.first_flight}`}</p>
                    <p>{`Type: ${data.dragon.type}`}</p>
                    <p>{`Dry mass: ${data.dragon.dry_mass_kg} kg`}</p>
                </div>
            )
        }
    };

    handleShowDetails = () => {

        this.setState({
            showDetails: !this.state.showDetails
        })
    };

    renderDragonItem = () => {
        const { data } = this.props;

        return (
                <div>
                    <h2 style={styles.title}>{data.dragon.name}</h2>
                    <p>{data.dragon.description}</p>
                    <div
                        style={styles.link}
                        onClick={this.handleShowDetails}
                    >
                        Show details
                    </div>
                    {this.renderDetails()}
                    <div style={styles.imageContainer}>
                        {this.renderDragonImages()}
                    </div>
                </div>
            )
    };

    render() {
        const {data} = this.props;

        if (data.loading === true) {
            return <Loading />
        } else {
            return (

                <div style={styles.wrapper}>
                    <NavLink style={styles.link} to="/dragons">Return</NavLink>
                    <div style={styles.container}>
                        {this.renderDragonItem()}
                    </div>
                </div>
            )
        }
    }
}


const mapStateToProps = (state) => {
    return {
        data: state.dragons
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDragonItemData: data => dispatch(dragonsActions.getDragonItemData(data)),
    };
};


Dragon.propTypes = {
    data: PropTypes.object,
    getDragonItemData: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(Dragon);

