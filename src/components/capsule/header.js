/* eslint-disable */
import React, { Component } from 'react';
import 'react-table/react-table.css'
import './index.css'

const style = {
    paddingBottom: 20,
    backgroundColor: '#282828',
    color: '#673ab7'
};

const Header = () => {
        return (
            <div style={style}>
                <h1 className="logo">
                    <span className="word1">The</span>
                    <span className="word2">Capsules</span>
                </h1>
            </div>
        )
};

export default Header;

