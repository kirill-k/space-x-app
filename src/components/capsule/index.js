/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactTable from "react-table";
import Header from './header';
import Loading from '../common/loader';
import { connect } from 'react-redux';
import { renderDateTime } from '../../utils'
import * as capsuleActions from '../../actions/capsule';
import 'react-table/react-table.css'


class Capsule extends Component {

    componentDidMount = () => {

        this.props.getCapsuleData()
    };

    render() {

        const { data } = this.props;
        const style = {
            padding: 20,
            width: '80%',
            margin: 'auto',
            marginTop: 60
        };

        const columns = [{
            Header: 'Capsule ID',
            accessor: 'capsule_id'
        }, {
            Header: 'Serial',
            accessor: 'capsule_serial'
        }, {
            id: 'original_launch',
            Header: 'Launch',
            accessor: t => renderDateTime(t.original_launch)
        }, {
            Header: 'Details',
            accessor: 'details'
        }, {
            Header: 'Status',
            accessor: 'status'
        }];

        if (data.loading === true) {
            return <Loading />
        } else {
            return (

                <div style={style}>
                    <Header />
                    <ReactTable
                        data={data.capsule}
                        columns={columns}
                    />
                </div>
            )
        }
    }

}


const mapStateToProps = (state) => {
    return {
        data: state.capsule
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCapsuleData: data => dispatch(capsuleActions.getCapsuleData(data)),
    };
};


Capsule.propTypes = {
    data: PropTypes.object,
    getCapsuleData: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(Capsule);

