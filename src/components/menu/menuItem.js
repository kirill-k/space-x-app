/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

class MenuItem extends Component {


    render(){

        const styles = {
            container: {
                opacity: 0,
                animation: '1s appear forwards',
                animationDelay: this.props.delay,
            },
            menuItem: {
                fontFamily:`'Open Sans', sans-serif`,
                fontSize: '1.2rem',
                padding: '1rem 0',
                margin: '0 5%',
                cursor: 'pointer',
                transition: 'color 0.2s ease-in-out',
                animation: '0.5s slideIn forwards',
                animationDelay: this.props.delay,

            },
            line: {
                width: '90%',
                height: '1px',
                background: 'gray',
                margin: '0 auto',
                animation: '0.5s shrink forwards',
                animationDelay: this.props.delay,

            }
        };

        return(
            <div style={styles.container}>
                <div
                    style={styles.menuItem}
                    onClick={this.props.onClick}
                >
                    <NavLink to={this.props.path}>{this.props.name}</NavLink>
                </div>
                <div style={styles.line}/>
            </div>
        )
    }
}

MenuItem.propTypes = {
    onClick: PropTypes.func,
    to: PropTypes.string,
    name: PropTypes.string
};


export default MenuItem;
