/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Menu extends Component {

    componentWillReceiveProps(nextProps){
        if(nextProps.open !== this.props.open){
            this.setState({ open: nextProps.open });
        }
    }

    render(){

        const styles={
            container: {
                position: 'fixed',
                top: 0,
                left: 0,
                height: this.props.open? '100%': 0,
                width: '100vw',
                display: 'flex',
                flexDirection: 'column',
                background: 'black',
                opacity: 0.95,
                color: '#fafafa',
                transition: 'height 0.3s ease',
                zIndex: 11,
            },
            menuList: {
                paddingTop: '6rem',
            }
        };

        return(
            <div style={styles.container}>
                {
                    this.props.open ?
                        <div style={styles.menuList}>
                            {this.props.children}
                        </div>:null
                }
            </div>
        )
    }
}

Menu.propTypes = {
    children: PropTypes.array.isRequired,
    open: PropTypes.bool
};


export default Menu;