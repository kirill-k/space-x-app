/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../index.css'
import MenuItem from './menuItem';
import HeaderMenu from '../header/index';
import Menu from './menu';

class MenuMain extends Component {
    state = {
        menuOpen: false
    };

    handleMenuClick = () => {
        this.setState({ menuOpen: !this.state.menuOpen });
    };

    handleLinkClick = () => {
        this.setState({ menuOpen: false });
    };

    render(){

        const styles = {
            container:{
                position: 'fixed',
                top: 0,
                left: 0,
                zIndex: '99',
                opacity: 0.9,
                display:'flex',
                justifyContent: 'space-between',
                alignItems:'center',
                background: 'black',
                width: '100%',
                color: 'white',
                fontFamily:'Lobster',
                height: '60px',
                boxShadow: '4px 2px 10px #343434'
            },
            body: {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                width: '100vw',
                filter: this.state.menuOpen ? 'blur(2px)':null,
                transition: 'filter 0.5s ease'
            },
        };

        return(
            <div>
                <div style={styles.container}>
                    <HeaderMenu open={this.state.menuOpen} onClick={this.handleMenuClick} color='white'/>
                </div>
                <Menu open={this.state.menuOpen}>
                    <MenuItem
                        delay='0.2s'
                        path='/'
                        name='Home'
                        exact='true'
                        onClick={this.handleLinkClick}
                    />
                    <MenuItem
                        delay='0.4s'
                        path='/capsules'
                        name='Capsules'
                        exact='true'
                        onClick={this.handleLinkClick}
                    />
                    <MenuItem
                        delay='0.6s'
                        path='/dragons'
                        name='Dragons'
                        exact='true'
                        onClick={this.handleLinkClick}
                    />
                    <MenuItem
                        delay='0.8s'
                        path='/contact'
                        name='Contacts (Coming Soon)'
                        exact='true'
                        onClick={this.handleLinkClick}
                    />
                </Menu>
            </div>
        )
    }
}

MenuMain.propTypes = {
    open: PropTypes.bool,
    onClick: PropTypes.func,
    delay: PropTypes.string,
    path: PropTypes.string,
    name: PropTypes.string
};


export default MenuMain;