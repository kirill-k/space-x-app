/* eslint-disable */
import React from 'react';
import { Link } from 'react-router-dom';

const style = {
    title: {
        fontSize: 100,
        textAlign: 'center'
    },
    text: {
        color: '#000'
    },
    block: {
        padding: 20,
        width: '80%',
        margin: 'auto',
        marginTop: 60
    },
    image: {
        width: 400,
        height: 400,
        display: 'block',
        margin: 'auto',
        position: 'relative'
    }
};

const NotFound = () => (

    <div style={{...style.block}}>
        <h1 style={{...style.title}}>404</h1>
        <img
            src='https://images.vexels.com/media/users/3/152650/isolated/preview/dc67bc351ada9cb55254534c2a35f57d-spacex-spacecraft-icon-by-vexels.png'
            style={{...style.image}}
        />
        <center>
            <Link style={{...style.text}} to="/">Return to Home Page</Link>
        </center>
    </div>
);


export default NotFound;