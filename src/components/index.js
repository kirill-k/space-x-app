/* eslint-disable */
import React, { Component } from 'react';
import {Routes} from '../routes'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from "react-redux";
import store from '../redux/store';
import MenuMain from './menu';
import '../index.css';



class Root extends Component {

    render() {

        return (
            <Provider store={store}>
                <BrowserRouter>
                    <div>
                        <MenuMain />
                        <Switch>
                            {
                                Routes.map((route, index) => {
                                    return <Route key={index} {...route} />
                                })
                            }
                        </Switch>
                    </div>
                </BrowserRouter>
            </Provider>
        )
    }

}

export default Root;

