/* eslint-disable */
import React, { Component } from 'react';
import store from '../../redux/store';
import '../../index.css';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';


class Favorite extends Component {
    state = {
        open: false
    };

    render() {

        const storage = store.getState();
        const dragonsFromStorage = storage.dragons.localStorage;
        const styles = {
            image: {
                width: 50,
                marginRight: 30
            },
            favoritesBlock: {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                cursor: 'pointer'
            },
            favoriteItems: {
                paddingRight: 10,
                fontSize: 22
            }
        };

        return (
            <NavLink to='/favorites'>
                <span style={{...styles.favoritesBlock}}>
                    <span style={{...styles.favoriteItems}}>
                        {dragonsFromStorage.length!==0 ? dragonsFromStorage.length : null}
                    </span>
                    <img
                        style={{...styles.image}}
                        src="http://www.iconarchive.com/download/i98340/dakirby309/simply-styled/Mac-Launchpad.ico"
                        alt="favorite"
                    />
                </span>
            </NavLink>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        data: state.dragons
    };
};


export default connect(mapStateToProps, null)(Favorite);

