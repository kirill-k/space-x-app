/* eslint-disable */
import React, { Component } from 'react';
import Favorite from './favorites';
import { NavLink } from 'react-router-dom';
import '../../index.css';


class HeaderMenu extends Component {
    state = {
        open: false
    };

    handleClick = () =>{
        this.setState({open: !this.state.open});
    };

    render() {

        const styles = {
            container: {
                height: '44px',
                width: '44px',
                display:'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                cursor: 'pointer',
                position: 'relative',
                left: '20px'
            },
            line: {
                height: '2px',
                width: '20px',
                background: '#fff',
                transition: 'all 0.2s ease',
            },
            lineTop: {
                transform: this.props.open ? 'rotate(45deg)':'none',
                transformOrigin: 'top left',
                marginBottom: '5px',
            },
            lineMiddle: {
                opacity: this.props.open ? 0: 1,
                transform: this.state.open ? 'translateX(-16px)':'none',
            },
            lineBottom: {
                transform: this.props.open ? 'translateX(-1px) rotate(-45deg)':'none',
                transformOrigin: 'top left',
                marginTop: '5px',
            },
            logo: {
                width: 250
            },
            image: {
                width: 50,
                marginRight: 30
            }
        };

        return (
            <>
                <div className="header_section">
                    <div style={styles.container}
                         onClick={this.props.onClick}>
                        <div style={{...styles.line,...styles.lineTop}}/>
                        <div style={{...styles.line,...styles.lineMiddle}}/>
                        <div style={{...styles.line,...styles.lineBottom}}/>
                    </div>
                    <span>
                        <NavLink
                            to='/'
                        >
                            <img
                                style={{...styles.logo}}
                                src="https://www.logolynx.com/images/logolynx/56/5617d9ea2e4fbd888d147c004d8b396b.png"
                            />
                        </NavLink>
                    </span>
                </div>
                <span>
                    <Favorite />
                </span>
            </>
        )
    }
}



export default HeaderMenu;

