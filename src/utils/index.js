export const renderDateTime = (data) => {
    if (data){
        let date = new Date(data);
        return date.toLocaleDateString() + ' ' + date.toLocaleTimeString()
    }
};