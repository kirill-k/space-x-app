import { GET_DATA_REQUESTED, CAPSULE_SUCCESS, CAPSULE_ERROR } from '../constants/capsule';

const rootsInitialState = {
    loading: false,
    loaded: false,
    capsule: [],
    errors: []
};


const capsule = ( state = rootsInitialState, action) => {
    switch( action.type ){
        case GET_DATA_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case CAPSULE_ERROR:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.data
            };

        case CAPSULE_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                capsule: action.data
            };
        default:
            return state;
    }
};

export default capsule;
