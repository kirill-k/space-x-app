import { combineReducers } from 'redux';
import capsule from './capsule'
import dragons from './dragons/dragons'

const reducer = combineReducers({
    capsule,
    dragons
});

export default reducer;
