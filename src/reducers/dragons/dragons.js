import {
    GET_DRAGONS_REQUESTED,
    DRAGONS_SUCCESS,
    DRAGON_SUCCESS,
    DRAGON_TO_LOCAL_STORAGE,
    DRAGON_FROM_LOCAL_STORAGE,
    DRAGONS_ERROR } from '../../constants/dragon';

const rootsInitialState = {
    loading: false,
    loaded: false,
    dragons: [],
    localStorage: [],
    dragon: {},
    errors: []
};



const dragons = ( state = rootsInitialState, action) => {
    switch( action.type ){
        case GET_DRAGONS_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case DRAGONS_ERROR:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.data
            };

        case DRAGONS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                dragons: action.data
            };
        case DRAGON_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                dragon: action.data
            };
        case DRAGON_TO_LOCAL_STORAGE:
            return {
                ...state,
                loading: false,
                loaded: true,
                localStorage: action.data
            };
        case DRAGON_FROM_LOCAL_STORAGE:
            return {
                ...state, localStorage: [...state.localStorage].filter((item)=> {
                    return item !== action.data
                })
            };
        default:
            return state;
    }
};

export default dragons;
